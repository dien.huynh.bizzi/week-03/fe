export const BASE_URL = "http://localhost:5000/graphql";

export const API_ENDPOINTS = {
  // Auth
  AUTH_LOGIN: "",
  AUTH_LOGOUT: "",
  AUTH_FORGOT_PASSWORD: "auth/forgot-password",
  AUTH_RESET_PASSWORD: "auth/reset-password",
  AUTH_REFRESH_TOKEN: "auth/refresh-token",
};
