import axios from "axios";
import { API_ENDPOINTS, BASE_URL } from "@/constants";

const instance = axios.create({
  baseURL: BASE_URL,
  withCredentials: true,
});

instance.interceptors.request.use(async (req) => {
  const token = localStorage.getItem("accessToken");
  if (token) {
    req.headers.Authorization = `Bearer ${token}`;
  }
  return req;
});

instance.interceptors.response.use(
  async (res) => {
    return res;
  },
  async (error) => {
    const retryRequestOriginal = error.config;

    if (error.response.status === 401 && !retryRequestOriginal._retry) {
      // flag _retry prevent infinity loop
      retryRequestOriginal._retry = true;
      // refresh token
      try {
        const refreshToken = localStorage.getItem("refreshToken");
        const reqRefreshToken = await instance.post(API_ENDPOINTS.AUTH_REFRESH_TOKEN, {
          refreshToken: refreshToken,
        });
        const newAccessToken = reqRefreshToken.data.AccessToken;
        const newRefreshToken = reqRefreshToken.data.RefreshToken;
        localStorage.setItem("accessToken", newAccessToken);
        localStorage.setItem("refreshToken", newRefreshToken);

        // recall api
        return instance(retryRequestOriginal);
      } catch (error) {
        // in case RefreshToken expired.
        localStorage.removeItem("accessToken");
        location.assign("/login");
        return Promise.reject(error);
      }
    }
    return Promise.reject(error);
  }
);

type typeRequest = "get" | "post" | "put" | "delete" | "patch";

export const request = async (type: typeRequest, api: string, payload?: any) => {
  const request = await instance[type](api, payload);
  return request;
};
