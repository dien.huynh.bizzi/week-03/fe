"use client";
import Link from "next/link";
import React, { useContext } from "react";
import styles from "./navbar.module.css";
import Image from "next/image";
import { DarkModeToggle } from "@/components";
import { AuthContext } from "@/context";
import { API_ENDPOINTS } from "@/constants";
import { request } from "@/services";
import { notification } from "antd";
import { useRouter } from "next/navigation";

const Navbar = () => {
  const route = useRouter();
  const { currentUser, setCurrentUser }: any = useContext(AuthContext);

  const handleLogout = async () => {
    const dataQuery = {
      query: `mutation LogoutMutation{
        logout{
            msg
        }
      }`,
    };

    const res = await request("post", API_ENDPOINTS.AUTH_LOGOUT, dataQuery);
    const { data, errors } = res.data;

    if (data.logout === null) {
      notification.error({
        message: "Failure to logout",
        description: `${errors[0].message}`,
        placement: "top",
      });
    }

    if (data.logout !== null) {
      localStorage.removeItem("accessToken");
      localStorage.removeItem("refreshToken");
      setCurrentUser(null);

      notification.success({
        message: `${data.logout.msg}`,
        placement: "top",
      });

      route.push("/");
    }
  };

  return (
    <div className={styles.container}>
      <Link href={"/"} className={styles.logo}>
        <Image src="/Bizzi-Logo-300x103.png" alt="logo-bizzi" width={150} height={51.5} />
      </Link>

      <div className={styles.links}>
        <DarkModeToggle />

        <Link href={"/"} className={styles.link}>
          Home
        </Link>
        {currentUser ? (
          <>
            <Link href={"/profile"} className={styles.link}>
              {currentUser.username}
            </Link>
            <Link onClick={handleLogout} href={"/"} className={styles.link}>
              Logout
            </Link>
          </>
        ) : (
          <Link href={"/login"} className={styles.link}>
            Login
          </Link>
        )}
      </div>
    </div>
  );
};

export default Navbar;
