"use client";
import React, { useContext } from "react";
import Link from "next/link";
import styles from "./page.module.css";
import { request } from "@/services";
import { API_ENDPOINTS } from "@/constants";
import { useRouter } from "next/navigation";
import { Button, Checkbox, Form, Input, notification } from "antd";
import { FieldLoginType } from "@/types";
import { AuthContext } from "@/context";

const Login = () => {
  const route = useRouter();
  const { setCurrentUser }: any = useContext(AuthContext);

  const onFinish = async (values: FieldLoginType) => {
    const dataQuery = {
      query: `mutation LoginMutation($user: LoginInput!){
        login(user: $user){
            msg
            id
            username
            accessToken
            refreshToken
          }
      }`,
      variables: {
        user: values,
      },
    };

    const res = await request("post", API_ENDPOINTS.AUTH_LOGIN, dataQuery);
    const { data, errors } = res.data;

    if (data.login === null) {
      notification.error({
        message: "Failure to login",
        description: `${errors[0].message}`,
        placement: "top",
      });
    }

    if (data.login !== null) {
      localStorage.setItem("accessToken", data.login.accessToken);
      localStorage.setItem("refreshToken", data.login.refreshToken);
      setCurrentUser({ id: data.login.id, username: data.login.username });

      notification.success({
        message: `${data.login.msg}`,
        placement: "top",
      });

      route.push("/");
    }
  };

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Login</h1>
      <Form
        name="login"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item<FieldLoginType>
          label="Username"
          name="username"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item<FieldLoginType>
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item<FieldLoginType>
          name="isRemember"
          initialValue={true}
          valuePropName="checked"
          wrapperCol={{ offset: 8, span: 16 }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className={styles.button}>
            Login
          </Button>
        </Form.Item>
      </Form>
      <span className={styles.or}>- OR -</span>
      <Button
        onClick={() => {
          console.log("first");
        }}
        htmlType="button"
        className={styles.button + " " + styles.google}
      >
        Login
      </Button>
      <Link className={styles.link} href="/register">
        Create new account
      </Link>
    </div>
  );
};

export default Login;
